import {HANDLE_ADD, HANDLE_MINUS} from './actionTypes';

export const handleAdd = {
    type : HANDLE_ADD,
};

export const handleMinus = {
    type : HANDLE_MINUS,
};
