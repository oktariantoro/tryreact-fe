import { combineReducers } from 'redux';
import counterReducer from './counter/reducer';
import authReducer from './auth/reducer';
import contriReducer from './contributor/reducer';

const rootReducer = {
    counter:counterReducer,
    auth:authReducer,
    contri:contriReducer,
}

export default combineReducers(rootReducer);