import {LOG_IN, LOG_OUT} from './actionTypes';

export const loginAction = (data) => {
    return {
        type : LOG_IN,
        data,
    }
};

export const logoutAction ={
    type : LOG_OUT,
};

