import axios from 'axios';
import {GET_CONTRI_REQUEST,GET_CONTRI_SUCCESS,GET_CONTRI_ERROR } from './actionTypes'

export const getContriReq = (payload) =>{
    return{
        type: GET_CONTRI_REQUEST,
        payload,
    };
};

export const getContriSuccess = (data) =>{
    return{
        type: GET_CONTRI_SUCCESS,
        data,
    };
};

export const getContriErr = (error) =>{
    return{
        type: GET_CONTRI_ERROR,
        error,
    };
};

export const getContriData = () => (dispatch) => {
    dispatch (getContriReq(true));
    axios.get('https://reqres.in/api/users?page=1').then((Response) => {
        console.log('res contri =>', Response);
        const data = Response.data.data;
        dispatch(getContriSuccess(data));
        dispatch(getContriReq(false));
    }).catch((error)=>{
        console.log('error contri=>', error);
        dispatch(getContriErr(error));
        dispatch(getContriReq(false));
    })
}
