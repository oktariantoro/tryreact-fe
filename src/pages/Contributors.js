import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';
import { getContriData } from '../redux/contributor/action';
import { Spinner } from 'reactstrap';




const Contributors = () => {
    const dispatch = useDispatch();
    const contriData = useSelector((states)=> states.contri.data); 
    const loading = useSelector((states) => states.contri.loading);
    const isAuth = useSelector((states) => states.auth.isAuthenticated);
    const error = useSelector((states) => states.contri.error);
    
    const [users, setUsers] = useState([]);
    useEffect(() => {
        //taruk kode ambil data
        //disini terjadi componentDidMount dan componentDidUpdate
        //function fetchData () {} //hoisting
        const fetchData = async () =>{
            const result = await axios('https://reqres.in/api/users?page=2');
            setUsers(result.data.data);//data spesifik yg akan ditampilkan
        };

        fetchData();
        dispatch(getContriData());
    }, [dispatch]);    //fungsi array kosong untuk willUnmount
    console.log ('user =>', users);
    console.log ('contridata =>', contriData);
    
    //const map1 = () =>{ users.map()};
    return (
        <>
        <h2>big thanks to ....</h2>
        
            {users.map((list, index) => (
                    <ul key={index}>
                    <li>{list.email}</li>
                    <li>{list.first_name}</li>
                    <img alt='gambar' src={list.avatar} />
                    </ul>
            ))}
        <br />
        <br />
        
        <div>----------------------------------</div>
        {loading ? <Spinner color="secondary" /> : contriData.map((el,k)=>(
            <div key={k}>
                <p>{el.email}</p>
                <img src={el.avatar} alt ="gambar"/>
            </div>
        ))}
        </>
        
    );
};

export default Contributors;