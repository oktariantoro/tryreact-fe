import React, {useState} from 'react';
import { Container, Row, Col, Card, Form, FormGroup, Label, Input, CardBody,
    CardTitle, Button } from 'reactstrap';
import '../App.css';
import {connect} from 'react-redux';
import {loginAction,logoutAction} from '../redux/auth/action';
import {useHistory} from 'react-router-dom';
import qs from 'qs';
import axios from 'axios';


const Login = (props) => {
    const[logData, setLogData] = useState({
        email:'',
        password:''
    });

    const handleChangeLog = (e) => {
        setLogData({
            ...logData,
            [e.target.name] : e.target.value
        })
    };
    const [token, setToken] = useState({});

    const handleSubmitLog = (e) => {
        e.preventDefault();
        console.log('kliksubmit')
        
        axios({
            method: 'post',
            url: 'https://reqres.in/api/login',
            data: qs.stringify({
              email : logData.email,
              password: logData.password,
            }),
            headers: {
              'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
          }).then((response)=>{
              console.log('response =>', response);
              setToken(response.data); 
              if (Object.keys(response.data).length !== 0) {
                  logInFunc()
                };
          })
    
          if (isLogin === true) {
            console.log("login false");
            logOutFunc();
        }
    };

    const {isLogin, logInFunc, logOutFunc} = props;
    const history = useHistory();

    const handleClick = ()=> {
        if(isLogin === true){
            logOutFunc(); 
            history.push('/');
        } else {
            logInFunc();
            history.push('/counter');
        }
    }
    
    return(
        
        <Container className="mt-4">
            <Row>
                <Col sm="20" md={{ size: 4, offset: 4 }} >
                    <Card>
                        
                        <CardBody>
                        <CardTitle tag="h5" className="text-center">LOGIN</CardTitle>
                        
                        <Form>
                            <FormGroup className="mb-2">
                                <Label for="exampleEmail">Email or Username</Label>
                                <Input type="email" name="email" id="exampleEmail" placeholder="Email or Username" onChange={handleChangeLog}/>
                            </FormGroup>
                            <FormGroup className="mb-2">
                                <Label for="examplePassword">Password</Label>
                                <Input type="password" name="password" id="examplePassword" placeholder="password" onChange={handleChangeLog} />
                            </FormGroup>
                            <FormGroup check>
                                <Label check>
                                <Input type="checkbox" />{' '}
                                Check me in
                                </Label>
                            </FormGroup>
                        </Form>
                        <Button onClick={handleSubmitLog} className="mt-2">Login</Button>
                        {/* <Button onClick={handleClick} className="mt-2">{isLogin ? 'Logout' : 'Login'}</Button> */}
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
};
const mapStateToProps= (globalState) => ({
    isLogin: globalState.auth.isAuthenticated
});
const mapDispatchToProps = (dispatch) =>({
    logInFunc:() => dispatch(loginAction),
    logOutFunc: () => dispatch(logoutAction), 
})

export default connect(mapStateToProps, mapDispatchToProps)(Login);