import React, { useState } from 'react';
import { Button } from 'reactstrap';
import { connect} from 'react-redux';
import {handleAdd, handleMinus} from '../redux/counter/action';

const Counter = (props) => {
    console.log('tes');
  const { count, handleAdd, handleMinus, isAuth } = props;
  const [click, setClick] = useState(100);
  const [nama, setNama] = useState('Bambang');

  // function klikGan() {
  //   alert('tes yuk');
  // }

  const klikGan = () => {
    alert('tes yuk yuk yuk');
  };

  console.log('props =>', props);

  //function klikGanJuga() {
    //alert('tes klik gan juga');
  //};

    return (
        <div className="App">
            <div className="judul">Tes 123</div>
            <button onClick={klikGan}>Yuk Klik</button>

            <div>Counter App pakai state biasa (useState)</div>
            <p>Anda ngeklik {click} kali</p>
            <Button className="button-counter" color="primary" onClick={() => setClick(click + 2)}>Klik +</Button>
            <Button color="secondary" onClick={() => setClick(click - 2)}>Klik -</Button>


            <div>------------------</div>
            {/* <Card nama={nama} /> */}
            <p>Ini Kartu</p>
            <p>{nama}</p>
            <button onClick={() => setNama('mas Rahadian')}>Ubah nama!</button>
            <br/>
            <br/>
            
            <div>Counter app menggunakan redux</div>
            <p>kamu ngeclick sebanyak {props.countGan} kali</p>
            <Button onClick={props.handleMinusFunc} style ={{marginRight : '10px'}} color = "danger"> - </Button>
            <Button onClick={props.handleAddFunc} color = "primary"> + </Button>
        
            <div>---------------------</div>
            <p>{isAuth.toString()}</p>
        </div>

    )
}

const mapStateToProps = (globalState)=> {
  return {
    countGan : globalState.counter.count,
    isAuth: globalState.auth.isAuthenticated
  };
};

const mapDispatchToProps = (dispatch) =>{
  return {
    handleAddFunc : () => dispatch(handleAdd),
    handleMinusFunc : () => dispatch(handleMinus),
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(Counter);