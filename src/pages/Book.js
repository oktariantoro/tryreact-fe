import React, {useState} from 'react';
import imgBook from '../assets/images/book.jpg';
import { Button } from 'reactstrap';
import { connect } from 'react-redux';


const Book = (props) => {
    
    const [data,setData] = useState([]);

    const handleAdd = () =>{
        //alert('handle add')
        const newData = {
            id : data.length+1 , nama : `buku ${data.length+1} `, img:imgBook
        };
        const newArr = [...data];
        const result = newArr.concat(newData);
        setData(result);
    }
    const handleRemove = () =>{
        //alert('handle remove')
        //const newData = {
          //  id : data.length+1 , nama : 'buku'
        //};
        const newArr = [...data];
        //const index = data.length;
        const result = newArr.slice(0,-1);
        setData(result);
    }
    return (
        <>
            <p>Counter : {props.countGan}</p>
            <p>pilih salah satu</p>
            <Button color="primary" onClick={handleAdd}>Add book</Button>
            <Button color="primary" onClick={handleRemove}>Remove book</Button>
            {data.map ((List, index) => (
                <ul key={index}>
                    <li>{List.id}</li>
                    <li>{List.nama}</li>
                    <img src={List.img} alt="gambar" />
                </ul>
            ))}

        </>
        
    );
};

const mapStateToProps = (globalState)=> {
    return {
      countGan : globalState.counter.count,
    };
  };

export default connect(mapStateToProps)(Book);