import React, { useState } from 'react';
//import '../src/assets/css/hooks.css';


const Hooks = () => {
    const [nama, setNama] = useState('tono');
    console.log(nama);
    //setNama ('andi');

    const ubahNama = () => {
        
        //setNama ('andi');
        if(nama === 'tono'){
            setNama ('andi');
        }else {
            setNama ('tono');
        }
    };

    return (
        <>
            <p>hooks pertama</p>
            <p className={nama === 'andi' ? "ubahwarnaBlue" : "ubahwarnaRed"}>nama nya adalah {nama}</p>
            <button onClick={ubahNama}>Ubah nama!</button>
        </>
    );
};

export default Hooks;