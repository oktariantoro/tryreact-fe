import React, {useState} from 'react';
import { Container, Row, Col, Card, Form, FormGroup, Label, Input, FormText, CardBody,
    CardTitle, Button } from 'reactstrap';
import qs from 'qs';
import axios from 'axios';
//import '../App.css';

const Register = (props) => {
    const[regData, setRegData] = useState({
        email:'',
        password:''
    });
    const [data, setData] = useState({});

    const handleChangeReg = (e) => {
        setRegData({
            ...regData,
            [e.target.name] : e.target.value
        })
    };
    const handleSubmitReg = (e) => {
        e.preventDefault();
        console.log('kliksubmit')
        axios({
            method: 'post',
            url: 'https://reqres.in/api/register',
            data: qs.stringify({
              email : regData.email,
              password: regData.password,
            }),
            headers: {
              'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
          }).then((response)=>{
              console.log('response =>', response);
              setData(response.data); 
          })

    };

    console.log('data =>', data);
    return(
        
        <Container className="mt-3 mb-3">
            <Row>
                <Col sm="20" md={{ size: 4, offset: 4 }} >
                    <Card>
                        
                        <CardBody>
                        <CardTitle tag="h5" className="text-center">REGISTER</CardTitle>
                        
                        <Form>
                            <FormGroup className="mb-2">
                                <Label for="email">Email<span classname="Red">*</span></Label>
                                <Input type="email" name="email" id="email" placeholder="Email" 
                                onChange={handleChangeReg}
                                />

                            </FormGroup>
                            {/* <FormGroup className="mb-2">
                                <Label for="address">Address</Label>
                                <Input type="address" name="address" id="address" placeholder="Address" />
                            </FormGroup> */}
                            <FormGroup className="mb-2">
                                <Label for="Password">Password*</Label>
                                <Input type="password" name="password" id="Password" placeholder="password" 
                                onChange={handleChangeReg}
                                />
                            </FormGroup>
                            {/* <FormGroup className="mb-2">
                                <Label for="Password2">Confirmation Password*</Label>
                                <Input type="password2" name="password2" id="Password2" placeholder="password2" />
                            </FormGroup>
                            <FormGroup check>
                                <Label check>
                                <Input type="checkbox" />{' '}
                                Check me in
                                </Label>
                            </FormGroup> */}
                            <Button onClick={handleSubmitReg} className="mt-2" type="submit">Register</Button>
                        </Form>

                        {/* <p>{data.id}</p> */}
                        {Object.keys(data).length === 0 ? null : "Anda telah sukses terdaftar"}
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}

export default Register;