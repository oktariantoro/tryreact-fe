//import logo from './logo.svg';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter, Switch, Route} from 'react-router-dom';
import AboutMe from './pages/AboutMe';
import Home from './pages/Home';
import Navbar from './components/Navbar';
import Hooks from './pages/Hooks';
import Contributors from './pages/Contributors';
import Book from './pages/Book';
import Counter from './pages/Counter';
import Login from './pages/Login';
import Register from './pages/Register';
import './assets/images/homer.jpg';
import ProtectedRoute from './components/ProtectedRoute';



//import '';

function App() {
  return (
    <BrowserRouter>
    <Navbar />
    <Switch>
      <Route exact path = "/" component={Home} />

      <ProtectedRoute path = "/about-me" component={AboutMe} />

      <Route path = "/hooks" component={Hooks} />

      <Route path = "/contributors" component={Contributors} />

      <Route path = "/book" component={Book} />

      <Route path = "/counter" component={Counter} />

      <Route path = "/login" component={Login} />

      <Route path = "/register" component={Register} />

    </Switch>
    </BrowserRouter>
  );
}

export default App;
