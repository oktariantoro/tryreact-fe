import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
  } from 'reactstrap';
  import '../App.css';
  import {logoutAction} from '../redux/auth/action'

  import {connect} from 'react-redux';


const Navbars = (props) =>{
    const {isLogin, logOutFunc} = props;
    const [isOpen, setIsOpen] = useState(false);
    const handleLogout= () => {logOutFunc()};
    const toggle = () => setIsOpen(!isOpen);
    return (
        
        <div>
        <Navbar dark expand="md">
            <NavbarBrand href="/" className='mx-2'>Profile FE</NavbarBrand>
            <NavbarToggler onClick={toggle} />
            <Collapse isOpen={isOpen} navbar>
            <Nav className="mr-auto" navbar>
                <NavItem>
                <NavLink><Link to="/">Home</Link></NavLink>
                </NavItem>
                <NavItem>
                <NavLink href="https://gitlab.com/oktariantoro">GitLab</NavLink>
                </NavItem>
                <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                    <span>More</span>
                </DropdownToggle>
                <DropdownMenu right>
                    <DropdownItem>
                        <Link to="/about-me">About me</Link>
                    </DropdownItem>
                    <DropdownItem>
                        <Link to="/hooks">Hooks</Link>
                    </DropdownItem>
                    <DropdownItem>
                        <Link to="/contributors">Contributors</Link>
                    </DropdownItem>
                    <DropdownItem>
                        <Link to="/book">Book</Link>
                    </DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem>
                        <Link to="/counter">Counter</Link>
                    </DropdownItem>
                </DropdownMenu>
                </UncontrolledDropdown>
                <NavItem><NavLink> <Link to="/login">{isLogin === false ? "Login" : null}</Link></NavLink></NavItem>
                
                <NavItem><NavLink onClick={handleLogout}>{isLogin === true ?"Logout" : null}</NavLink></NavItem>
                <NavItem><NavLink> <Link to="/register">Register</Link></NavLink></NavItem>
            </Nav>
            </Collapse>
        </Navbar>
        </div>


        //<div>
          // <Link to="/">Home |</Link> 
          // <Link to="/about-me">About me |</Link>
          // <Link to="/hooks">Hooks |</Link>
          // <Link to="/contributors">Contributors |</Link>
          // <Link to="/book">Book |</Link>
          // <Link to="/counter">Counter</Link>
        //</div>
    );
}
const mapStateToProps= (globalState) => ({
    isLogin: globalState.auth.isAuthenticated
});
const mapDispatchToProps = (dispatch) =>({
    // logInFunc:() => dispatch(loginAction),
    logOutFunc: () => dispatch(logoutAction), 
})

export default connect(mapStateToProps, mapDispatchToProps)(Navbars);
