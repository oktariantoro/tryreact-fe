import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const ProtectedRoute = ({isAuth, component : Component, ...rest}) => {
    return(
        <Route 
            {...rest}
            render={routeProps=>{
                if(isAuth === true){
                    return <Component {...routeProps}/>
                } else {
                    return (
                        <Redirect to="/login" />
                    )
                }
            }}
        />
    )
}
const mapStateToProps = (globalState)=> {
    return {
      isAuth: globalState.auth.isAuthenticated
    };
  };

export default connect(mapStateToProps)(ProtectedRoute);
