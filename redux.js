const redux = require('redux');

//buat initial state
const initialState ={
    nilai:25,
    umur:30,
}
//action type
const TAMBAH_UMUR = 'TAMBAH_UMUR';
const TAMBAH_NILAI = 'TAMBAH_NILAI';
// buat reducer
const rootReducer = (state = initialState, action) => {
    //if (action.type === TAMBAH_UMUR){
      //  return {
        //    ...state,
          //  umur : state.umur + 60,
        //}
    //} else if (action.type === TAMBAH_NILAI){
      //  return {
        //    ...state,
          //  nilai : state.nilai + action.newData,
        //}
    //}
    //return state;

    switch(action.type){
        case TAMBAH_UMUR:
            return {
                ...state,
                umur : state.umur + 60,
            }
        
        case TAMBAH_NILAI:
            return {
                ...state,
                nilai : state.nilai + action.newData,
            }
       
        default:
            return state;
    }
};

// buat store
const store = redux.createStore(rootReducer);

// buat action
store.dispatch( {type:TAMBAH_UMUR} );
store.dispatch( {type:TAMBAH_NILAI, newData:75});
console.log('get store =>', store.getState());
